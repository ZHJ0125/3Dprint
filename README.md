# 3Dprint

<img width="350" height="500" src="https://github.com/MakerLab308/3Dprint/blob/master/4_%E8%B5%84%E6%BA%90%E6%96%87%E4%BB%B6/3D_Rick.jpg"/>

Figure1 Rick

<img width="480" height="350" src="https://github.com/MakerLab308/3Dprint/blob/master/4_%E8%B5%84%E6%BA%90%E6%96%87%E4%BB%B6/3D_DogAndMan.JPG"/>

Figure2 Dog & Man

# 如何使用本仓库?
本仓库仅作为3D打印入门学习使用

主要包括 [使用说明](https://github.com/MakerLab308/3Dprint/tree/master/1_%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E)、
[调试教程](https://github.com/MakerLab308/3Dprint/tree/master/2_%E8%B0%83%E8%AF%95%E6%95%99%E7%A8%8B)、
[模型文件](https://github.com/MakerLab308/3Dprint/tree/master/3_%E6%A8%A1%E5%9E%8B%E6%96%87%E4%BB%B6)、
[资源文件](https://github.com/MakerLab308/3Dprint/tree/master/4_%E8%B5%84%E6%BA%90%E6%96%87%E4%BB%B6) 四部分,请按照标题顺序进行学习




## 1_使用说明
包含3D打印的概述及使用方法的简单介绍,可以作为3D打印的初步了解

## 2_调试教程
包含Prusa i3和Ender3打印机的详细教程,从打印机的组装到软件的使用方法,介绍的非常详细!

## 3_模型文件
包含商家提供的3D模型以及我们积攒的各种3D模型压缩文件,还包括商家的3D打印效果展示(没有我们打印的效果好😂)

## 4_资源文件
包含3D打印的延时摄影视频、OctoPrint中文包、模型展示等资源
