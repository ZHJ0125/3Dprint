## 资源文件

### timelapse
包含了由树莓派OctoPrint软件,在打印过程中进行延时摄影的四个视频

### zh_CN.zip
这个压缩包是OctoPrint软件汉化语言包

- #### Octoprint汉化方法
- 1.在OctoPrint软件 `Octoprint Setting -->  外观 --> 管理语言包` 界面中,选择上传

- 2.将zh_CN.zip上传至OctoPrint软件

- 3.重启OctoPrint服务

![image](https://github.com/MakerLab308/3Dprint/blob/master/4_%E8%B5%84%E6%BA%90%E6%96%87%E4%BB%B6/setChinese_for_OctoPrint.png)
