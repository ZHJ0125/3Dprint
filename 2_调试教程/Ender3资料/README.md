# Ender3 教程

## 1.Installing
包含Ender3打印机组装调试教程

## 2.Software and USB Driver
包含Cura软件及部分驱动程序

## 3.Trouble Shootings
提供常见问题的解决办法,`建议使用前先阅读一下`

## 4.Model
包含少量用于测试的模型文件,其余模型文件见本仓库的[`3_模型文件`](https://github.com/MakerLab308/3Dprint/tree/master/3_%E6%A8%A1%E5%9E%8B%E6%96%87%E4%BB%B6)

## 5.Ender_file
包含Ender3打印机的用户手册、质保须知等文件
