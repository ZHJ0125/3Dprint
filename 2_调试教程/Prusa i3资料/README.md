# Prusa i3资料
**教程以自己思考为主，技术支持为辅！**


## 目录介绍：
#### 1.安装与调试
包含打印机主板接线、打印平台调平、软件调试等教程

#### 2.驱动程序
包含打印机连接电脑时所需安装的驱动程序,如果安装失败请安装驱动精灵尝试解决

#### 3.软件
包含电脑端需要安装的控制软件.<br>
由于文件大小限制，只给出软件下载地址

#### 4.模型
包含用于简单测试的模型文件,以及`组装该Prusa i3打印机所需的Gcode文件`!

#### 5.固件源码
包含`Marlin固件`源码及编译好的hex文件，供烧录（刷机）<br>
建议不要轻易尝试刷机...

#### 6.主板原理图
包含主控板原理图,主板接线图等,`供专业人士参考`！

#### 7.建模教程
包含犀牛和soildworks两个建模软件的视频教程。

#### 8.网络教程收集
包含 Cura 软件的简单教程
