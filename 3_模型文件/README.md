# 关于3D打印的网页收藏

## 一、3D模型网站

- ### Thingiverse	(强力推荐) https://www.thingiverse.com/

- ### YouMagine https://www.youmagine.com/

- ### 打印啦 http://www.dayin.la/cn/product/category.html

- ### 各网站介绍 http://mp.ofweek.com/3dprint/a645643028776

## 二、3D建模学习网站

- ### Autodesk 
http://help.autodesk.com/view/fusion360/ENU/?guid=GUID-1C665B4D-7BF7-4FDF-98B0-AA7EE12B5AC2

## 三、打印照片网站

- ### Image to Lithophane http://3dp.rocks/lithophane/

## 四、模型如何上色

- ### 3d打印如何上色 https://www.zhihu.com/question/28919308
